<?php

namespace App\Http\Controllers;

use App\Events\StoryViewedEvent;
use App\Stories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;

class StoriesController extends Controller
{
    public function index()
    {
        $query = Stories::orderByDesc('time');
        if (request()->filled('type')){
            switch (request()->get('type')) {
                case "newstories":
                    $query->where('type', Stories::NEW_STORIES);
                    break;
                case "topstories":
                    $query->where('type', Stories::TOP_STORIES);
                    break;
                case "beststories":
                    $query->where('type', Stories::BEST_STORIES);
                    break;
                default:
                    $query->whereIn('type', Stories::BEST_STORIES, Stories::BEST_STORIES, Stories::NEW_STORIES);
            }
        }

       $stories = $query->simplePaginate(30);

       return view('stories.index', compact('stories'));
    }

    public function show($id)
    {
       $story = Stories::findOrfail($id);

       return view('stories.show', compact('story'));
    }

    public function viewStory($id)
    {
        $story = Stories::findOrfail($id);
        event(new StoryViewedEvent($story));
    }
}
