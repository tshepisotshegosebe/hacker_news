<?php

namespace App\Http\Controllers;

use App\Service\ImportNewsRequestService;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    /**
     * @var ImportNewsRequestService
     */
    private $requestService;

    public function __construct(ImportNewsRequestService $requestService)
    {
        $this->requestService = $requestService;
    }

    public function show($author)
    {
        $user = $this->requestService->getAuthor($author);

        return view('author.show', compact('user'));
    }
}
