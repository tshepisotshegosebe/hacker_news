<?php

namespace App\Service;

class AgeService
{
    private $time;

    public function __construct(int $itemTime)
    {
        $this->time = $itemTime;
    }

    public function getAge(): string
    {
        $time = $this->time;
        $time = time() - $time; // to get the time since that moment
        $time = ($time < 1) ? 1 : $time;
        $rules = [
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        ];

        foreach ($rules as $key => $value) {
            if ($time > $key){
                $diff = floor($time / $key);
                $withS = ($diff > 1) ?'s': '';
                return "$diff $value$withS";
            }
        }
    }
}
