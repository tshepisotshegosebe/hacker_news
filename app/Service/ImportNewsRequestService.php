<?php

namespace App\Service;

use App\Comments;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise\EachPromise;
use GuzzleHttp\Psr7\Response;

class ImportNewsRequestService
{
    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://hacker-news.firebaseio.com/v0/',
            'defaults' => [
                'headers'  => ['content-type' => 'application/json', 'Accept' => 'application/json'],
            ],

        ]);
    }

    /**
     * Importing stories
     *
     * @param array $items
     * @param int   $type
     * @param       $progressBar
     */
    public function storeNewsItems(array $items, int $type, $progressBar)
    {
        $progressBar->start();

        /*
        * use promises to make multiple/parallel asynchronous requests to speed up the process
        */
        $promises = (function () use ($items) {
            foreach ($items as $item) {
                yield $this->client->getAsync("item/$item.json?print=pretty");
            }
        })();

        $eachPromise = new EachPromise($promises, [
            'concurrency' => 5,
            'fulfilled' => function (Response $response) use ($type, $progressBar){
                if ($response->getStatusCode() == 200) {
                    $story = json_decode($response->getBody(), true);
                    \DB::beginTransaction(); //safely perform DB queries
                    if (isset($story['id'])){ // we have no use of deleted posts unless otherwise
                        try {
                            //insert into table stories
                            \DB::table('stories')->insert([
                                'id' => $story['id'],
                                'title' => $story['title'],
                                'author' => $story['by'],
                                'story_url' => $story['url'] ?? '',
                                'text' => $story['text'] ?? '',
                                'type' => $type,
                                'score' => $story['score'],
                                'time' => $story['time'],
                                'comment_count' => $story['descendants'] ?? 0,
                            ]);
                            \Log::debug("Done importing story id: {$story['id']}");
                            $progressBar->advance();
                        }catch (\Exception $exception){
                            \Log::error($exception->getMessage());
                        }
                    }

                    //if the story has kids import them also
                    if (isset($story['kids']) && $count = count($story['kids']) > 0){
                        $this->storeComments($story['kids']);
                        \Log::debug("Done importing $count comments for id: {$story['id']}");
                    }
                    \DB::commit(); // close transaction
                }
            },
            'rejected' => function (RequestException $exception) {
                \Log::error($exception->getMessage());
            }
        ]);

        $eachPromise->promise()->wait();

    }

    /**
     * Only get story ids
     *
     * @param string $story
     *
     * @return array
     */
    public function getItemIds(string $story): array
    {
        $request = $this->client->get("$story.json?print=pretty");

        return json_decode($request->getBody());
    }

    /**
     * Import comments (kids)
     *
     * @param $kids
     */
    private function storeComments($kids)
    {
        $existingComments = Comments::pluck('id')->toArray();
        $commentsNeeded = array_diff($kids, $existingComments); // only get new comments

        /*
         * use promises to make multiple/parallel asynchronous requests to speed up the process
         */
        $promises = (function () use ($commentsNeeded) {
            foreach ($commentsNeeded as $kid) {
                yield $this->client->getAsync("item/$kid.json?print=pretty");
            }
        })();

        $eachPromise = new EachPromise($promises, [
            // how many concurrency we are use
            'concurrency' => 10,
            'fulfilled' => function (Response $response) {
                if ($response->getStatusCode() == 200) {
                    $comment = json_decode($response->getBody(), true);

                    if (!isset($comment['deleted']) && isset($comment['id'])){ // we have no use of deleted comments unless otherwise
                        try {
                            //insert into table comments
                            \DB::table('comments')->insert([
                                'id' => $comment['id'],
                                'text' => $comment['text'] ?? '',
                                'author' => $comment['by'],
                                'parent_id' => $comment['parent'],
                                'time' => $comment['time'],
                            ]);
                        }catch (\Exception $exception){
                            \Log::error($exception->getMessage());
                        }
                    }

                    if (isset($comment['kids']) && count($comment['kids']) > 0){
                        /*
                         * The reason its calling itself if there are kids is because i do not know the level of replies
                         * so calling itself will be able to fetch nested replies
                         */
                        $this->storeComments($comment['kids']);
                    }
                }
            },
            'rejected' => function (RequestException $exception) {
                \Log::error($exception->getMessage());
            }
        ]);
        $eachPromise->promise()->wait();
    }

    /**
     * Get Item Author
     *
     * @param $author
     *
     * @return array|null
     */
    public function getAuthor($author): ?array
    {
        $request = $this->client->get("user/$author.json?print=pretty");

        return json_decode($request->getBody(), true);
    }

}
