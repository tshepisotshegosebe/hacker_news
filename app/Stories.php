<?php

namespace App;

use App\Service\AgeService;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Stories
 *
 * @property int $id
 * @property string $title
 * @property string $author
 * @property string $text
 * @property string $story_url
 * @property int $type
 * @property int $score
 * @property int $time
 * @property int $comment_count
 * @property bool $viewed
 * @property-read string $age
 * @property-read string $host
 */

class Stories extends Model
{
    const TOP_STORIES = 1;
    const NEW_STORIES = 2;
    const BEST_STORIES = 3;
    const NOT_VIEWED = 0;
    const VIEWED = 1;

    protected $table = 'stories';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['id','title','author','story_url','type', 'score','time', 'comment_count', 'text', 'viewed'];
    protected $appends = ['age', 'host'];

    public function comments()
    {
        return $this->hasMany('App\Comments', 'parent_id', 'id')->orderByDesc('time');
    }

    public function getAgeAttribute(): string
    {
        return (new AgeService($this->time))->getAge();
    }

    public function getHostAttribute()
    {
        if ($this->story_url != ''){
            return parse_url($this->story_url)['host'];
        }
    }
}
