<?php

namespace App\Events;

use App\Stories;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StoryViewedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Stories
     */
    public $story;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Stories $story)
    {
        $this->story = $story;
    }
}
