<?php

namespace App\Listeners;

use App\Events\StoryViewedEvent;
use App\Stories;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MarkStoryViewed
{

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(StoryViewedEvent $event)
    {
        $story = $event->story;
        if ($event->story->viewed === Stories::NOT_VIEWED){
            $story->viewed = Stories::VIEWED;
            $story->save();
        }
    }
}
