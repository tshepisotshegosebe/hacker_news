<?php

namespace App\Console\Commands;

use App\Service\ImportNewsRequestService;
use App\Stories;
use Illuminate\Console\Command;

class ImportNewsCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Hacker News';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ImportNewsRequestService $service
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getStories(Stories::TOP_STORIES, 'topstories');
        $this->getStories(Stories::BEST_STORIES, 'beststories');
        $this->getStories(Stories::NEW_STORIES, 'newstories');
    }

    private function getStories(int $type, string $apiType)
    {
        $existingStories = Stories::pluck('id')->toArray();
        $service = new ImportNewsRequestService();
        $bestStories = $service->getItemIds($apiType);
        $newsNeeded = array_slice(array_diff($bestStories, $existingStories), 0, 500); // get only 500 new beststories
        $progressBar = $this->output->createProgressBar(count($newsNeeded));
        $service->storeNewsItems($newsNeeded, $type, $progressBar);
        \Log::debug('Done importing '.count($newsNeeded).' '.$apiType);
        $progressBar->finish();
    }
}
