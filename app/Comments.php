<?php

namespace App;

use App\Service\AgeService;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Comments
 * @property int $id
 * @property string $text
 * @property string $author
 * @property int $parent_id
 * @property int $time
 * @property-read  string $comment_age
 *
 * @package App
 */
class Comments extends Model
{
    protected $table = 'comments';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['id','text','author','parent_id','time'];
    protected $appends = ['comment_age'];

    public function story()
    {
        return $this->belongTo('App\Stories', 'id', 'parent_id');
    }

    public function replies()
    {
        return $this->hasMany(Comments::class, 'parent_id');
    }

    public function getCommentAgeAttribute()
    {
        return (new AgeService($this->time))->getAge();
    }
}
