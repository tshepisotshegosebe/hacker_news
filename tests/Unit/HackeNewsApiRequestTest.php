<?php

namespace Tests\Unit;

use GuzzleHttp\Client;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HackeNewsApiRequestTest extends TestCase
{
    private $http;

    protected function setUp(): void
    {
        parent::setUp();
        $this->http = new Client([
            'base_uri' => 'https://hacker-news.firebaseio.com/v0/',
            'defaults' => [
                'headers'  => ['content-type' => 'application/json', 'Accept' => 'application/json'],
            ],
        ]);
    }

    public function testGetItemIds()
    {
        $response = $this->http->get('topstories.json?print=pretty');
        $this->assertEquals(200, $response->getStatusCode());// response is a success
        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json; charset=utf-8", $contentType); //contentType is application/json
        $this->assertTrue(is_array(json_decode($response->getBody()))); //is response an array
    }

    public function testGetAuthor($author = 'aficionado')
    {
        $response = $this->http->get("user/$author.json?print=pretty");
        $this->assertEquals(200, $response->getStatusCode());// response is a success
        $results = json_decode($response->getBody());
        $this->assertTrue(is_object($results)); //is response an object
        $this->assertTrue(property_exists($results, 'about')); //contains about
        $this->assertTrue(property_exists($results, 'created'));//contains created
        $this->assertTrue(property_exists($results, 'karma'));//contains karma
        $this->assertTrue(property_exists($results, 'id'));//contains id
    }

    public function testNewsItemRequest()
    {
        $topStoryResponse = $this->http->get('topstories.json?print=pretty');
        $topStoryResult = json_decode($topStoryResponse->getBody());
        $item = current($topStoryResult);

        $response = $this->http->get("item/$item.json?print=pretty");
        $this->assertEquals(200, $response->getStatusCode());// response is a success
        $results = json_decode($response->getBody());

        $this->assertTrue(is_object($results)); //is response an object
        $this->assertTrue(property_exists($results, 'by')); //contains by
        $this->assertTrue(property_exists($results, 'descendants'));//contains descendants
        $this->assertTrue(property_exists($results, 'id'));//contains id
        $this->assertTrue(property_exists($results, 'kids'));//contains kids
        $this->assertTrue(property_exists($results, 'score'));//contains score
        $this->assertTrue(property_exists($results, 'time'));//contains time
        $this->assertTrue(property_exists($results, 'title'));//contains title
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->http = null;
    }
}
