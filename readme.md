

# Hacker News

## Setup

- clone the [hackernews development setup](https://bitbucket.org/tshepisotshegosebe/hacker_news_dev_setup/src/master/).
- install [docker compose](https://docs.docker.com/compose/install/).
- insert the path to your projects and path to your databases in docker compose.yml .eg. /media/mosa/work/Projects.
- clone my [hacker news repository](https://bitbucket.org/tshepisotshegosebe/hacker_news/src/master/) in a different directory though.
- insert ***127.0.0.1 hackernews.local*** into */etc/hosts*.
- From the development setup directory run ***docker-compose up --build*** on your terminal.
- Create new database hacker_news in PhpMyAdmin which should be located at ***localhost:8080***.
- Create .env file from the .env.example file, they should be axactly the same.
- After creating .env file run ***php artisan key:generate*** from the terminal to gererate apllication key.
- From the hacker news ide terminal/directory run ***php artisan migration*** to generate migration tables.
- on your browser go to ***hackernews.local***
