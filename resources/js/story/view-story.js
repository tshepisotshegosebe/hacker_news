$('.story-link').on('click', (e) => {
    e.preventDefault();
    let url = routes.viewStory.replace('-1', e.target.id); // replace default -1 with story id
    $.post(url)
    .done(function() {
        window.location.href = e.target.href;
    }).fail((xhr, status, error) => {
        console.log(xhr.responseText)
    });
});

$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
});
