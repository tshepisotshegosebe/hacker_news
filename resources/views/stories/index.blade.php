@extends('layouts.app')
@section('title', 'Stories')
@push('head')
    <style>
        a { color: #000000; }
    </style>
@endpush
@section('content')

    <div class="card my-3">
        <div class="card-body table-responsive p-0">
            @include('stories.partials._hacker_news_table', ['stories' => $stories])
            {{ $stories->links() }}
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        var routes = {
                viewStory : "{!! route('view.story', -1) !!}",
            }
    </script>
    <script src="{{mix('/js/story/view-story.js')}}"></script>
@endpush

