<table class="table table-hover table-sm" style="background-color: #f6f6ef">
    <tbody>
    <tr>
        <td style="background-color: #ff6600">
            <table class="table-borderless">
                <tbody>
                <tr>
                    <td><a href="/"><img src="https://news.ycombinator.com/y18.gif" style="border:1px solid #ffffff;"></a></td>
                    <td>
                        <span>
                            <span>
                                <a href="/" class="font-weight-bold">Hacker News</a>
                            </span>
                             {!! link_to_route('news.index', 'new', ['type' => 'newstories'], ['target' => '_blank']) !!}
                            |
                            {!! link_to_route('news.index', 'best', ['type' => 'beststories'], ['target' => '_blank']) !!}
                            |
                            {!! link_to_route('news.index', 'top', ['type' => 'topstories'], ['target' => '_blank']) !!}
                            |
                            <a href="">comments</a>
                        </span>
                    </td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr class="h-10"></tr>
    <tr>
        <td>
            @isset($stories)
                @include('stories.partials._story_header', ['stories' => $stories])
            @endisset
            @isset($user)
                @include('author._show_author', ['user' => $user])
            @endisset
            @isset($comments)
                <div class="col-5">
                    {!! Form::textarea('reply',null,['class'=>'form-control', 'rows' => 2, 'cols' => 40]) !!}
                    {!! Form::button('add comment', ['class' => 'my-3']) !!}
                </div>
                <? /** @var \App\Comments $comment */  ?>
                @include('comments._show_comment', ['comments' => $comments])
            @endisset
        </td>
    </tr>
    </tbody>
</table>
