<table class="table-borderless">
    <tbody>
    <?php /** @var \App\Stories $story */ ?>
    @foreach($stories as $story)
        <tr>
            <td>
                @if(count($stories) > 1)
                    <span>{{ ($stories ->currentpage()-1) * $stories ->perpage() + $loop->iteration}}</span>
                @endif
            </td>
            <td><a href=""><i class="fas fa-1x fa-caret-up"></i></a></td>
            <td>
                <a href="{{$story->story_url}}" id="{{$story->id}}"
                   class="story-link {{$story->viewed == \App\Stories::VIEWED ? 'text-muted' : ''}}">{{$story->title}}</a>
                <?php $host = isset($story->host) ? '('.link_to($story->story_url, $story->host, ['class' => 'text-muted']).')' : '' ?>
                <span> {!! $host  !!}</span>
                @if(count($stories) == 1)
                    <p class="my-2">{!! $story->text !!}</p>
                @endif
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td class="small">
                <span>{{$story->score}} {{$story->score == 1 ? 'point' : 'points'}}</span>
                by
                <a href="{{route('show.author',$story->author)}}">{{$story->author}}</a>
                <span> <a href="{{route('news.show', $story->id)}}">{{$story->age}}</a></span>
                <span></span>
                |
                <a href="">hide</a>
                |
                {{link_to("https://hn.algolia.com/?query={$story->title}", 'past')}}
                |
                {{link_to("http://www.google.com/search?q={$story->title}", 'web')}}
                |
                <a href="{{route('news.show', $story->id)}}">discuss</a>
                @if($story->comment_count > 0)
                    |
                    <a href="{{route('news.show', $story->id)}}">{{$story->comment_count}}{{$story->comment_count == 1 ? ' comment' : ' comments'}} </a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

