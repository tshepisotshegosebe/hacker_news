@foreach($comments as $comment)
    <div class="ml-3">
        <div>
            <span>
                <i class="fas fa-caret-up"></i>
                <a href="" class="text-muted">{{$comment->author}}</a>
                <span>
                    <a href="" class="text-muted">{{$comment->comment_age}} [-]</a>
                </span>
            </span>
        </div>
        <div>
            <span>{!! $comment->text !!}</span>
            <p><a href="">reply</a></p>
        </div>
    </div>
    <div class="ml-5">
    @include('comments._show_comment',['comments' => $comment->replies])
    </div>
@endforeach
