@extends('layouts.app')
@section('title', 'Stories')
@push('head')
    <style>
        a { color: #000000; }
    </style>
@endpush
@section('content')
    <div class="card my-3">
        <div class="card-body table-responsive p-0">
            @include('stories.partials._hacker_news_table',['user', $user])
        </div>
    </div>
@endsection
