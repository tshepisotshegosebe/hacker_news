<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('/news');
});
Route::resource('/news', 'StoriesController')->only('index', 'show');
Route::get('/author/{id}', 'AuthorController@show')->name('show.author');
Route::post('view/{id}', 'StoriesController@viewStory')->name('view.story');

